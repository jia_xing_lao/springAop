package com.offcn.cglib;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class MyProxy implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        if(method.getName().equals("test")){
            System.out.println("test之前");
            methodProxy.invokeSuper(o,objects);
            System.out.println("test之后");
        }else{
            methodProxy.invokeSuper(o,objects);
        }
        return null;
    }
}
