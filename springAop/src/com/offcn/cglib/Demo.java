package com.offcn.cglib;

import org.springframework.cglib.proxy.Enhancer;

public class Demo {
    public static void main(String[] args) {
        // 动态代理的核心类
        Enhancer eh = new Enhancer();
        // 找到需要修改方法所在的类
        eh.setSuperclass(PersonServcie.class);
        // 获取代理的类
        eh.setCallback(new MyProxy());
        // 代理以后返回的新的类
        PersonServcie service  = (PersonServcie) eh.create();
        // 在调用方法的时候，用代理对象给返回的类去操作
        service.test();
        service.run();
    }

}
