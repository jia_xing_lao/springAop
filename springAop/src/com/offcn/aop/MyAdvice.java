package com.offcn.aop;

import org.aspectj.lang.ProceedingJoinPoint;

public class MyAdvice {
    /**
     * 在对方法进行增强的时候，有几种方式可以进行增强
     * 前置通知（在原来的代码之前新增功能）
     * 后置通知（在原来的代码之后新增功能）
     * 环绕通知（在原来的代码的前后都新增功能）
     * 异常通知（在原来的代码出现异常的时候，才能添加新的功能）
     */

    void before(){
        System.out.println("前置通知");
    }

    void after(){
        System.out.println("后置通知");
    }

    Object huanrao(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("环绕的头");
        Object proceed = pjp.proceed();
        System.out.println("环绕的结束");
        return  proceed;
    }

    void excep(){
        System.out.println("异常通知");
    }
}
