package com.offcn.aop;

/**
    测试springAop切面编程
 **/

import com.offcn.service.AopService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        AopService service = (AopService) app.getBean("aopService");
//        service.save();
        service.delete();
    }
}
