package com.offcn.service.impl;

import com.offcn.service.AopService;

public class AopServiceImpl implements AopService {
    @Override
    public void save() {
        int i = 10;
        int j  = i / 0;
        System.out.println("这是一个save方法");
    }

    @Override
    public void update() {

        System.out.println("这是一个update方法");

    }

    @Override
    public void delete() {

        System.out.println("这是一个delete方法");
    }

    @Override
    public void test() {

        System.out.println("这是一个test方法");
    }
}
