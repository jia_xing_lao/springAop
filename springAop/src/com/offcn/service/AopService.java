package com.offcn.service;

public interface AopService {

    void save();
    void update();
    void delete();
    void test();
}
