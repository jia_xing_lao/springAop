package com.offcn.tx;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TxTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContext-tx.xml");
        PersonService service = (PersonService) app.getBean("personService");
        // 模拟转账
        Person p = new Person();
        p.setId(1);
        p.setMoney(0);
        service.updateCang(p);


        // 模拟收款
        p.setMoney(2000);
        p.setId(2);
        service.updateZe(p);
    }
}
