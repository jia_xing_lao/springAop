package com.offcn.aopZhujie;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
public class AopAdvice {

    @Before("execution(* com.offcn.aopZhujie.UserServiceImpl.save(..))")
    void before(){

        System.out.println("前置通知");
    }

    @After("execution(* com.offcn.aopZhujie.UserServiceImpl.save(..))")
    void after(){

        System.out.println("后置通知");
    }


}
