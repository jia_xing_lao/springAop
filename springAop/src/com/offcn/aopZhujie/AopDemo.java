package com.offcn.aopZhujie;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AopDemo {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContextaop.xml");
       UserService service = (UserService) app.getBean("userService");
       service.dele();
       service.save();
    }
}
