package com.offcn.jdk;

import java.lang.reflect.Proxy;

public class Demo {

    public static void main(String[] args) {
        // 需要修改的接口中的方法
        UserServcie userServcie = new UserServiceImpl();
        // 自定义的代理类
        MyProxy myProxy = new MyProxy(userServcie);
        UserServcie servcie = (UserServcie) Proxy.newProxyInstance(
                userServcie.getClass().getClassLoader(),
                userServcie.getClass().getInterfaces(),
                myProxy);
       // 现在在调用接口中方法的时候，就用代理对象给返回的对象进行调用
       servcie.test();
       servcie.run();

    }
}
