package com.offcn.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyProxy implements InvocationHandler {

    private Object object;
    public MyProxy(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object invoke = null;

        if(method.getName().equals("test")){  // 准备修改的方法，加入修改的逻辑代码
            System.out.println("test之前");
            invoke = method.invoke(object, args);
            System.out.println("test之后");
        }else{ // 不准备修改的方法，就全部正常放行
            invoke = method.invoke(object, args);
        }
        return invoke;
    }
}
